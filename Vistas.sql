﻿USE teoria1;

/*
  1 Indicar el nombre de los coches que hayan alquilado.
*/

-- Crear una vista.

CREATE OR REPLACE VIEW consulta1 AS 
SELECT DISTINCT c.marca FROM coches c join alquileres a
  ON c.codigoCoche=a.coche;

-- Ejecutar la vista

  SELECT * FROM consulta1 c;

/* La misma optimizada
*/

  -- C1Consulta1: coches alquilados

  CREATE OR REPLACE VIEW c1Consulta1 AS
  SELECT DISTINCT a.coche
  FROM alquileres a;

  CREATE OR REPLACE VIEW consulta1 AS
  SELECT DISTINCT c.marca
  FROM
     c1Consulta1 c1
  JOIN coches c
  ON c1.coche=c.codigoCoche;

  -- Nombres de los usuarios que hayan alquilado alguna vez coches.

  -- Sin optimizar
  
  CREATE OR REPLACE VIEW consulta2 AS
  SELECT DISTINCT nombre
  FROM usuarios join alquileres a
  ON codigoUsuario = a.usuario;

-- optimizada
-- c1: Consulta1: Los usuarios que han alquilado coches 

SELECT DISTINCT a.usuario FROM alquileres a1;

-- Consulta combinada final con Vista.

CREATE OR REPLACE VIEW consulta2 AS
SELECT DISTINCT nombre 
FROM c1consulta2 c1
JOIN usuarios u
ON c1.usuario= u.codigoUsuario;

-- Coches que no han sido alquilados con una vista.

USE teoria1
CREATE OR REPLACE VIEW consulta3 as
SELECT c.codigoCoche FROM coches c
LEFT JOIN alquileres a ON c.codigoCoche = a.coche WHERE a.coche IS NULL;

-- Para que muestre un resultado la vista anterior escribimos esto:

SELECT * FROM consulta3 c;

-- La consulta optimizada sería:

-- c1 Sería el coche que se ha alquilado.
  SELECT DISTINCT a.coche FROM alquileres a;
-- final

  SELECT c.codigoCoche FROM coches c LEFT JOIN
    (
SELECT DISTINCT a.coche FROM alquileres a
    ) c1
    ON c.codigoCoche= c1.coche
    WHERE c1.coche IS NULL;
-- optimizada
CREATE OR REPLACE VIEW c1consulta3 as
SELECT DISTINCT coche FROM alquileres a;

CREATE OR REPLACE VIEW consulta3 AS
  SELECT c.codigoCoche FROM coches c
LEFT JOIN c1consulta3 c1
  ON c.codigoCoche = c1.coche
  WHERE c1.coche IS NULL
  

