﻿USE teoria1;

-- Consultas para repasar el left join.

  /* Indicar el nombre de las marcas que se hayan vendido coches
  */

-- Consulta sin optimizar

SELECT DISTINCT c.marca FROM coches c join alquileres a
  ON c.codigoCoche=a.coche;


-- Consulta optimizada

-- c1

SELECT a.coche FROM alquileres a;

-- consulta final

  SELECT DISTINCT c.marca
  FROM
  (
  SELECT DISTINCT a.coche
  FROM alquileres a
  ) c1
  JOIN coches c
  ON c1.coche=c.codigoCoche;

-- Coches que no han sido alquilados.

USE teoria1
SELECT c.codigoCoche FROM coches c
LEFT JOIN alquileres a ON c.codigoCoche = a.coche WHERE a.coche IS NULL;

-- optimizada

SELECT c.codigoCoche FROM coches c
LEFT JOIN (
  SELECT DISTINCT a.coche FROM alquileres a
           ) c1
  ON c.codigoCoche = c1.coche
  WHERE c1.coche IS NULL;

-- Usuarios que no han alguilado coches.

