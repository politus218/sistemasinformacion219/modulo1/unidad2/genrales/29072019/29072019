﻿USE teoria1;

-- Consultas para repasar el left join.

  /* Indicar el nombre de las marcas que se hayan vendido coches
  */

SELECT DISTINCT c.marca FROM coches c join alquileres a
  ON c.codigoCoche=a.coche;
